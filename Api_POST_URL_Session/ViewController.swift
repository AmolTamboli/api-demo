//
//  ViewController.swift
//  Api_POST_URL_Session
//
//  Created by Amol Tamboli on 19/03/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var txtUserId: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtBody: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func enterTapped(_ sender: Any) {
        self.setUpPostMethod()
    }
    
}

extension ViewController {
    func setUpPostMethod() {
        guard let uid = self.txtUserId.text else {return}
        guard let title = self.txtTitle.text else {return}
        guard let body = self.txtBody.text else {return}
        
        if let url = URL(string: "https://us-central1-sample93.cloudfunctions.net/app/api/read"){
            var request = URLRequest(url: url)
            
            //MARK:- if header required
            //   request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-type")
            
           request.httpMethod = "GET"
//
//            let parameters: [String : Any] = [
//                "userId" : uid,
//                "title" : title,
//                "body" : body
//            ]
//           request.httpBody = parameters.percentEscaped().data(using: .utf8)
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    if error == nil {
                        print(error?.localizedDescription ?? "Unknown Error")
                    }
                    return
                }
                
                if let response = response as? HTTPURLResponse{
                    guard (200 ... 209) ~= response.statusCode else {
                        print("Status Code:- \(response.statusCode)")
                        print(response)
                        return
                    }
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                } catch let error{
                    print(error.localizedDescription)
                }
            }.resume()
        }
    }
}

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}

